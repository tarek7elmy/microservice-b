FROM openjdk:8-jdk-alpine
COPY /target/MicroserviceB.jar ./MicroserviceB.jar
EXPOSE 8080:8080
ENTRYPOINT ["java","-jar","/MicroserviceB.jar"]