package com.dot.api;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class MainController {
	
	
	@RequestMapping("/api")
    public String home() {
        return "Hello MicroserviceB...";
    }
	
	
	
}
